class CreateTagContents < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :tag_type, null: false
      t.text :contents
      t.integer :url_id, null: false

      t.timestamps null: false
    end
  end
end
