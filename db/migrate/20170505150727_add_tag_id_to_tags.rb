class AddTagIdToTags < ActiveRecord::Migration
  def change
    add_column :tags, :tag_id, :integer

    add_index :tags, :tag_id
    add_index :tags, :url_id
  end
end
