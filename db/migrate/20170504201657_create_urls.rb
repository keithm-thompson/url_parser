class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.text :url, null: false

      t.timestamps null: false
    end

    add_index :urls, :url, unique: true
  end
end
