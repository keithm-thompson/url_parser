Rails.application.routes.draw do
  namespace :api, defaults: {format: :json} do
    get '/urls' => 'urls#index'
    post '/extract' => 'urls#create'
  end
end
