require 'rails_helper'

RSpec.describe Url, type: :model do

  subject(:test_url) { Url.create(url: Dir.pwd + "/spec/support/test.html") }

  describe 'validations' do
    it { should validate_presence_of(:url) }
    it { should validate_uniqueness_of(:url)}
  end

  describe 'associations' do
    it { should have_many(:tags).dependent(:delete_all) }
  end

  it "triggers #extract_contents on save" do
    url = Url.new(url: "hello")
    expect(url).to receive(:extract_contents)
    url.save
  end

  describe 'instance methods' do
    describe '#extract_contents' do
      it 'creates tags based on url html' do
        expect(test_url.tags).not_to be_empty
      end

      it 'saves created tags in database' do
        expect(Tag.where(url_id: test_url.id)).not_to be_empty
      end

      it 'doesn\'t generate tags from invalid url' do
        url = Url.create(url: "")
        expect(url.tags).to be_empty
      end
    end
  end
end
