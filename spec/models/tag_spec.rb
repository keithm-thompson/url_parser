require 'rails_helper'

RSpec.describe Tag, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:tag_type) }
    it { should validate_presence_of(:url) }
  end
  describe 'associations' do
    it { should belong_to(:url) }
  end
end
