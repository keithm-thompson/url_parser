require 'rails_helper'
require "#{Rails.root}/lib/helpers/urls_parser.rb"

RSpec.describe UrlsParser do
  describe '::parse_url' do

    it 'extracts tags from page' do
      tags = UrlsParser.parse_url(Dir.pwd + "/spec/support/test.html", ["h1", "h2", "h3", "a", "div"])
      expect(tags.length).to eq(5)
    end

    it 'doesn\'t extract unwanted tags' do
      tags = UrlsParser.parse_url(Dir.pwd + "/spec/support/test.html", ["h1"])
      tags.each do |tag|
        expect(["h1"]).to include tag[0]
      end
    end

    it 'extracts text from header tags' do
      tags = UrlsParser.parse_url(Dir.pwd + "/spec/support/test.html", ["h1", "h2", "h3"])
      tags.each do |tag|
        expect(tag[1]).not_to be_empty
      end
    end

    it 'extracts href from links' do
      links = UrlsParser.parse_url(Dir.pwd + "/spec/support/test.html", ["a"])
      link = links[0]
      expect(link[1]).to eq("https://www.test.com")
    end

    it 'returns nil if link is invalid' do
      tags = UrlsParser.parse_url("", "")
      expect(tags).to be_nil
    end
  end
end
