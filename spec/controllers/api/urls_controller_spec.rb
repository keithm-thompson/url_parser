require 'rails_helper'

RSpec.describe Api::UrlsController, type: :controller do
  before :each do
    request.env["HTTP_ACCEPT"] = 'application/json'
  end
  
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index

      expect(response).to have_http_status(200)
      expect(response).to render_template("api/urls/get_urls")
    end
  end

  describe "POST #create" do
    context "when given valid params" do
      it "responds with 204" do
        post :create, url: "test"
        expect(response).to have_http_status(204)
      end
    end

    context "when given duplicate url" do
      it "should save url as new" do
        post :create, url: "test"
        url = Url.find_by(url: "test")

        post :create, url: "test"
        url2 = Url.find_by(url: "test")

        expect(url.id).not_to eq(url2.id)
      end

      it "should delete old urls" do
        post :create, url: "test"
        post :create, url: "test"

        expect(Url.where(url: "test").length).to eq(1)
      end
    end

    context "when given invalid params" do
      it "responds with 404" do
        post :create, url: nil
      end
    end
  end

end
