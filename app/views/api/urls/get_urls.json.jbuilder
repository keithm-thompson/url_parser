json.array! @urls do |url|
  json.url url.url

  json.tags url.tags do |tag|
     json.tag_type tag.tag_type
     json.contents tag.contents
   end
end
