require_dependency "#{Rails.root}/lib/helpers/urls_parser.rb"

class Url < ActiveRecord::Base
  TAGS_DESIRED =  ["h1", "h2", "h3", "a"]

  validates :url, presence: true, uniqueness: true

  has_many :tags, dependent: :delete_all

  after_save :extract_contents

  def extract_contents
    contents = UrlsParser.parse_url(self.url, TAGS_DESIRED)
    return unless contents

    contents.each do |tag, text|
      new_tag = Tag.new(tag_type: tag, contents: text)
      new_tag.url = self
      new_tag.save
    end
  end
end
