class Tag < ActiveRecord::Base
  validates :tag_type, :url, presence: true

  belongs_to :url
end
