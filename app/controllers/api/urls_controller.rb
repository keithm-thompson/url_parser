class Api::UrlsController < ApplicationController
  def create
    # If url exists - delete it so that all tags are deleted and the contents are updated in database.
    # I considered manually checking for which contents were changed but that would be an extremely expensive operation in terms of both time and memory.
    # If we will be anticipating dealing with urls of more than 5000 tags in the database - I would have a background process do the deleting so that it doesn't lock
    url = Url.includes(:tags).find_by(url: params[:url])
    url.destroy if url

    url = Url.new(url: params[:url])
    if url.save
      #204 response
      head :no_content
    else
      render status: 404, json: url.errors.full_messages.to_json
    end
  end

  def index
    @urls = Url.includes(:tags).all
    render "api/urls/get_urls"
  end
end
