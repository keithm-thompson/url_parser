# URL_Parser API #
*RESTful Rails API with two endpoints:*

    GET '/urls'

    POST '/extract', params: url

To access the API, please clone the repository locally, move to the directory, run `bundle install`, and run `rails s`. After the server is running, you can use any URL you would like - the test one was provided as a small example:

`curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"url": "#{PATH_TO_DIR}/URL_Parser/spec/support/test.html"}'  http://localhost:3000/api/urls`

`curl -v -H "Accept: application/json" -H "Content-type: application/json" -X GET http://localhost:3000/api/urls`  

To run the tests provided, simply run `bundle exec rspec spec`

## Schema ##
* #### Url ####
    + id: integer (required)
    + url: string (required, indexed)

* #### Tag ####
    + id: integer (required
    + tag_type: string (required)
    + contents: string 
    + url_id: foreign_key (required, indexed)
    + tag_id: self-referencing foreign_key (indexed)
> *note: this column was added to show how the schema might evolve to handle more tags. If we would like to keep track of the entire DOM we could create the html tree structure out of self-joins*


## Controllers ##

* #### UrlsController ####
     + \#create
         - route: POST '/api/extract', params: url
         - responds with 204 on success
         - responds with 404 on error
     + \#index 
         - route: GET '/api/urls' 
         - format: json
         - json object returned of the form
```json
[{"url":"url","tags":[{"tag_type":"tag_type","contents":"contents"}]
```
## Helper Classes ##

* #### UrlsParser ####
     + ::parse_url(url, tags_desired)
        - returns array of the form [tag_type, contents]
        - returns nil if url is invalid 
        - utilizes [Nokogiri](https://github.com/sparklemotion/nokogiri) gem to parse HTML

## Tests ##

> A [spec](https://bitbucket.org/keithm-thompson/url_parser/src/89c2f8095abce9bcffe2a95cdf6f95e49a96bf4d/spec/?at=master) folder has been included with RSpec tests covering models, helper classes, and controllers.