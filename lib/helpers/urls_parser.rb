require 'nokogiri'
require 'open-uri'

class UrlsParser
  def self.parse_url(url, tags_desired)
    tags_found = []
    begin
      page = Nokogiri::HTML(open(url))
    rescue => e
      return
    end

    tags_desired.each do |html_tag|
      if html_tag == "a"
        page.css(html_tag).each do |link|
          tags_found << [html_tag, link.attributes["href"].to_s]
        end
      else
        page.css(html_tag).each do |html_el|
          tags_found << [html_tag, html_el.text]
        end
      end
    end

    tags_found
  end
end
